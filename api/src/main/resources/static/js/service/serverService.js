'use strict'

let serverServiceFn = function ($http, $rootScope) {
    this.loadServerInfo = function () {
        return $http.get($rootScope.basePath + '/server')
    };
}

appModule.service('serverService', serverServiceFn);