'use strict'

let modsServiceFn = function ($http, $rootScope) {
    this.loadMods = function () {
        return $http.get($rootScope.basePath + '/download/mods', {responseType: 'arraybuffer'})
    };

    this.loadMod = function (modName) {
        let param = {modName: modName}
        let config = {responseType: 'blob'};
        return $http.get($rootScope.basePath + '/download/mod', {
            params: param, headers: {
                'Accept': 'application/java-archive'
            },
            responseType: 'arraybuffer'
        })
    }
}

appModule.service('modsService', modsServiceFn);
