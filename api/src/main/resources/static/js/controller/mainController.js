'use strict'

appModule.controller('mainController', function ($scope, $rootScope, modsService, serverService) {
    $rootScope.basePath = 'http://143.47.183.17:8080' //Не забывай менять на 143.47.183.17 при пуше!
    $scope.showSpinnerStatus = false;

    let getBrowserName = function () {
        let userAgent = window.navigator.userAgent;

        let browsers = {chrome: /chrome/i, safari: /safari/i, firefox: /firefox/i, ie: /internet explorer/i};

        for (let key in browsers) {
            if (browsers[key].test(userAgent)) {
                return key;
            }
        }

        return 'unknown';
    }

    $scope.init = function () {
        serverService.loadServerInfo()
            .then(function (info) {
                $scope.server = info.data;
                $scope.serverStatus = info.data.status;
            })
    }

    $scope.loadMods = function () {
        $scope.showSpinnerStatus = true;
        modsService.loadMods()
            .then(function (modPack) {
                let file = new Blob([modPack.data], {type: 'application/octet-stream'});

                switch (getBrowserName()) {
                    case 'chrome':
                        let url = window.URL || window.webkitURL;

                        let downloadLink = angular.element('<a></a>');
                        downloadLink.attr('href', url.createObjectURL(file));
                        downloadLink.attr('target', '_self');
                        downloadLink.attr('download', 'mods.zip');
                        $scope.showSpinnerStatus = false;
                        downloadLink[0].click();
                        break;

                    default:
                        let fileURL = URL.createObjectURL(file);
                        $scope.showSpinnerStatus = false;
                        window.open(fileURL);
                }
            }, function () {
                $scope.showSpinnerStatus = false;
            })
    }

    $scope.loadMod = function (modName) {
        modsService.loadMod(modName)
            .then(function (mod) {
                let file = new Blob([mod.data], {type: 'application/java-archive'});

                switch (getBrowserName()) {
                    case 'chrome':
                        let url = window.URL || window.webkitURL;

                        let downloadLink = angular.element('<a></a>');
                        downloadLink.attr('href', url.createObjectURL(file));
                        downloadLink.attr('target', '_self');
                        downloadLink.attr('download', modName + '.jar');
                        downloadLink[0].click();
                        break;

                    default:
                        let fileURL = URL.createObjectURL(file);
                        window.open(fileURL);
                }
            }, function (response) {
                console.log(response);
            })
    }
});