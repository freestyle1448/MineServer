package ru.sultanyarov.mineserver.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.nio.file.Path;

@Getter
@Setter
@Validated
@ConfigurationProperties(prefix = "minecraft.server")
public class MinecraftServerProperties {
    /**
     * Адрес сервера
     */
    @NotNull
    private String serverHost;

    /**
     * Порт сервера
     */
    @NotNull
    @Positive
    private Integer serverPort;

    /**
     * Основная папка сервера
     */
    @NotNull
    private Path serverFolder;
}
