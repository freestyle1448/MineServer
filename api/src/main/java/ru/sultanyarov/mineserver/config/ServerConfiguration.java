package ru.sultanyarov.mineserver.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(MinecraftServerProperties.class)
public class ServerConfiguration {
}
