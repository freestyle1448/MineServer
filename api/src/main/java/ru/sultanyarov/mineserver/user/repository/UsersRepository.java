package ru.sultanyarov.mineserver.user.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import ru.sultanyarov.mineserver.user.domain.ServerUser;

public interface UsersRepository extends MongoRepository<ServerUser, ObjectId> {
    ServerUser findByName(String name);
}
