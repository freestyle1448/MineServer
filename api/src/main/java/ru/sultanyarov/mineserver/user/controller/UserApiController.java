package ru.sultanyarov.mineserver.user.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sultanyarov.mineserver.server.controller.AuthenticationApi;
import ru.sultanyarov.mineserver.user.ControlApi;
import ru.sultanyarov.mineserver.user.service.UserService;

import java.security.Principal;

@Slf4j
@Validated
@RestController
public class UserApiController implements ControlApi, AuthenticationApi {
    private final UserService userService;

    public UserApiController(UserService userService) {
        this.userService = userService;
    }

    @Override
    @RequestMapping("/login")
    public Principal user(Principal user) {
        return user;
    }
}
