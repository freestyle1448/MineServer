package ru.sultanyarov.mineserver.user.service;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.sultanyarov.mineserver.user.domain.ServerUser;
import ru.sultanyarov.mineserver.user.repository.UsersRepository;

import java.util.List;

import static java.util.List.of;

@Component
public class MongoUserDetailsService implements UserDetailsService {
    private final UsersRepository usersRepository;

    public MongoUserDetailsService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ServerUser serverUser = usersRepository.findByName(username);

        if (serverUser == null) {
            throw new UsernameNotFoundException("User not found");
        }

        List<SimpleGrantedAuthority> authorities = of(new SimpleGrantedAuthority("user"));

        return new User(serverUser.getName(), serverUser.getPassword(), authorities);
    }
}
