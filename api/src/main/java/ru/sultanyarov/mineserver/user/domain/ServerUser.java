package ru.sultanyarov.mineserver.user.domain;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "user")
public class ServerUser {
    @Id
    private ObjectId id;
    private String name;
    private String password;
}
