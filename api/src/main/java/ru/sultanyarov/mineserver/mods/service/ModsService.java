package ru.sultanyarov.mineserver.mods.service;

import net.lingala.zip4j.ZipFile;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import ru.sultanyarov.mineserver.config.MinecraftServerProperties;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;

@Service
public class ModsService {
    private final MinecraftServerProperties minecraftServerProperties;

    public ModsService(MinecraftServerProperties minecraftServerProperties) {
        this.minecraftServerProperties = minecraftServerProperties;
    }

    public File downloadMods() throws IOException {
        try (var zipFile = new ZipFile("mods.zip")) {
            zipFile.addFolder(new File(minecraftServerProperties.getServerFolder().resolve("mods").toUri()));
            return zipFile.getFile();
        }
    }

    public Resource downloadMod(String modName) throws FileNotFoundException {
        try {
            Path filePath = minecraftServerProperties.getServerFolder().resolve("mods").resolve(modName + ".jar").normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new FileNotFoundException("Mod not found " + modName);
            }
        } catch (MalformedURLException ex) {
            throw new FileNotFoundException("Mod not found " + modName);
        }
    }
}