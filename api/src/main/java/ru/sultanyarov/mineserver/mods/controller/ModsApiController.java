package ru.sultanyarov.mineserver.mods.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;
import ru.sultanyarov.mineserver.mods.DownloadApi;
import ru.sultanyarov.mineserver.mods.service.ModsService;

import java.io.FileNotFoundException;

import static org.springframework.http.ContentDisposition.builder;
import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.util.FileCopyUtils.copyToByteArray;

@Slf4j
@Validated
@RestController
public class ModsApiController implements DownloadApi {
    private final ModsService modsService;

    public ModsApiController(ModsService modsService) {
        this.modsService = modsService;
    }

    @Override
    public ResponseEntity<byte[]> downloadMods() {
        try {
            return ok().body(copyToByteArray(modsService.downloadMods()));
        } catch (Exception e) {
            log.error("Error while getting mods", e);

            return badRequest().build();
        }
    }

    @Override
    public ResponseEntity<Resource> downloadMod(String modName) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentDisposition(builder("inline")
                    .filename(modName)
                    .build());
            return ok()
                    .headers(headers)
                    .body(modsService.downloadMod(modName));
        } catch (FileNotFoundException e) {
            log.error("Error while getting mod {}\n{}", modName, e);

            return badRequest().build();
        }
    }
}
