package ru.sultanyarov.mineserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MineServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MineServerApplication.class, args);
    }
}
