package ru.sultanyarov.mineserver.server.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;
import ru.sultanyarov.mineserver.server.ServerApi;
import ru.sultanyarov.mineserver.server.dto.ServerInfo;
import ru.sultanyarov.mineserver.server.mapper.ServerApiMapper;
import ru.sultanyarov.mineserver.server.service.ServerInfoService;

import static org.springframework.http.ResponseEntity.ok;

@Slf4j
@Validated
@RestController
public class ServerApiController implements ServerApi {
    private final ServerApiMapper serverApiMapper;
    private final ServerInfoService serverInfoService;

    public ServerApiController(ServerApiMapper serverApiMapper, ServerInfoService serverInfoService) {
        this.serverApiMapper = serverApiMapper;
        this.serverInfoService = serverInfoService;
    }

    @Override
    public ResponseEntity<ServerInfo> getServerInfo() {
        return ok(serverApiMapper.domainServerInfoToDto(serverInfoService.getServerInfo()));
    }
}
