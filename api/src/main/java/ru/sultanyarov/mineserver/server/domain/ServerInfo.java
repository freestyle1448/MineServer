package ru.sultanyarov.mineserver.server.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class ServerInfo {
    private List<String> mods ;
    private List<String> players;
    private Integer playersCount;
    private Integer maxPlayersCount;
    private ServerStatus status;
}
