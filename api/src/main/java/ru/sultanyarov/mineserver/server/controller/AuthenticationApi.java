package ru.sultanyarov.mineserver.server.controller;


import java.security.Principal;

public interface AuthenticationApi {
    Principal user(Principal user);
}
