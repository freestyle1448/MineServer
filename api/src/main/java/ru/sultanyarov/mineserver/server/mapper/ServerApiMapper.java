package ru.sultanyarov.mineserver.server.mapper;

import com.github.steveice10.mc.auth.data.GameProfile;
import com.github.steveice10.mc.protocol.data.status.ServerStatusInfo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.sultanyarov.mineserver.server.domain.ServerInfo;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface ServerApiMapper {
    ru.sultanyarov.mineserver.server.dto.ServerInfo domainServerInfoToDto(ServerInfo serverInfo);

    @Mapping(target = "playersCount", source = "playerInfo.onlinePlayers")
    @Mapping(target = "players", source = "playerInfo.players")
    @Mapping(target = "maxPlayersCount", source = "playerInfo.maxPlayers")
    ServerInfo minecraftSessionToDomain(ServerStatusInfo serverStatusInfo);

    default List<String> gameProfilesToPlayers(GameProfile[] profiles) {
        var players = new ArrayList<String>();

        for (GameProfile profile : profiles) {
            players.add(profile.getName());
        }

        return players;
    }
}
