package ru.sultanyarov.mineserver.server.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ServerStatus {
    OFF("OFF"),
    ON("ON");

    private final String value;
}
