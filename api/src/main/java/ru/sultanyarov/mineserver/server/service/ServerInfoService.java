package ru.sultanyarov.mineserver.server.service;

import com.github.steveice10.mc.auth.service.SessionService;
import com.github.steveice10.mc.protocol.MinecraftConstants;
import com.github.steveice10.mc.protocol.MinecraftProtocol;
import com.github.steveice10.mc.protocol.data.status.handler.ServerInfoHandler;
import com.github.steveice10.packetlib.ProxyInfo;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.tcp.TcpClientSession;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.sultanyarov.mineserver.config.MinecraftServerProperties;
import ru.sultanyarov.mineserver.server.domain.ServerInfo;
import ru.sultanyarov.mineserver.server.mapper.ServerApiMapper;

import java.net.Proxy;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.stream.Stream;

import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.stream.Collectors.toList;
import static ru.sultanyarov.mineserver.server.domain.ServerStatus.OFF;
import static ru.sultanyarov.mineserver.server.domain.ServerStatus.ON;

@Slf4j
@Service
public class ServerInfoService {
    private static final ProxyInfo PROXY = null;
    private static final Proxy AUTH_PROXY = Proxy.NO_PROXY;

    private final ServerApiMapper mapper;
    private final MinecraftServerProperties minecraftServerProperties;

    public ServerInfoService(ServerApiMapper mapper, MinecraftServerProperties minecraftServerProperties) {
        this.mapper = mapper;
        this.minecraftServerProperties = minecraftServerProperties;
    }

    public ServerInfo getServerInfo() {
        var sessionCompletableFuture = getServerStatus();
        ServerInfo info;

        try {
            info = sessionCompletableFuture.get(10, SECONDS);
            info.setStatus(ON);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            log.error("Error while retrive data from server", e);

            info = ServerInfo.builder()
                    .status(OFF)
                    .build();
        }

        injectMods(info);

        return info;
    }

    @SneakyThrows
    private void injectMods(ServerInfo info) {
        try (Stream<Path> stream = Files.list(minecraftServerProperties.getServerFolder().resolve("mods"))) {
            info.setMods(stream
                    .filter(path -> path.toFile().getName().endsWith(".jar"))
                    .map(path -> path.toFile().getName().replace(".jar", ""))
                    .collect(toList()));
        }
    }

    private CompletableFuture<ServerInfo> getServerStatus() {
        CompletableFuture<ServerInfo> sessionCompletableFuture = new CompletableFuture<>();

        SessionService sessionService = new SessionService();
        sessionService.setProxy(AUTH_PROXY);

        MinecraftProtocol protocol = new MinecraftProtocol();
        Session client = new TcpClientSession(minecraftServerProperties.getServerHost(), minecraftServerProperties.getServerPort(), protocol, PROXY);
        client.setFlag(MinecraftConstants.SESSION_SERVICE_KEY, sessionService);
        client.setFlag(MinecraftConstants.SERVER_INFO_HANDLER_KEY, (ServerInfoHandler) (session, info) -> sessionCompletableFuture.complete(mapper.minecraftSessionToDomain(info)));

        client.connect();

        return sessionCompletableFuture;
    }
}
