package ru.sultanyarov.mineserver.mods.controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import ru.sultanyarov.mineserver.config.MinecraftServerProperties;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.util.FileSystemUtils.deleteRecursively;

@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test.properties")
@AutoConfigureMockMvc
class ModsApiControllerTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private MinecraftServerProperties serverProperties;
    private String modName;

    @BeforeEach
    void setUp() {
        try {
            Files.createDirectory(serverProperties.getServerFolder());
            Path generatedFolder = Files.createDirectory(serverProperties.getServerFolder().resolve("mods"));
            Path mod = Files.createTempFile(generatedFolder, "Mod", ".jar");
            modName = mod.getFileName().toString().replace(".jar", "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    void tearDown() throws IOException {
        deleteRecursively(serverProperties.getServerFolder());
    }

    @Test
    @DisplayName("Success getting all mods from server")
    void downloadMods() throws Exception {
        mvc.perform(
                get("/download/mods")
        )
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith("application/zip"));
    }

    @Test
    @DisplayName("Success getting mod by name")
    void downloadMod() throws Exception {
        mvc.perform(
                get("/download/mod")
                        .queryParam("modName", modName)
        )
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith("application/java-archive"));
    }

    @Test
    @DisplayName("Test error when try getting non-existent mod")
    void downloadNonExistentMod() throws Exception {
        ResultActions modName = mvc.perform(
                get("/download/mod")
                        .queryParam("modName", "nonExistentMod.jar")
        )
                .andExpect(status().isBadRequest());
    }
}