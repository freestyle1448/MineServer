package ru.sultanyarov.mineserver;

import com.github.cloudyrock.mongock.driver.mongodb.sync.v4.driver.MongoSync4Driver;
import com.github.cloudyrock.standalone.MongockStandalone;
import com.mongodb.client.MongoClients;

public class Migration {
    public static void main(String[] args) {
        var mongoClient = MongoClients.create("mongodb://localhost:27017/?retryWrites=true&w=majority");
        var runner = MongockStandalone.builder()
                .setDriver(MongoSync4Driver.withDefaultLock(mongoClient, "mineserver"))
                .addChangeLogsScanPackage("ru.sultanyarov.mineserver.changelog")
                .buildRunner();

        runner.execute();
    }
}
