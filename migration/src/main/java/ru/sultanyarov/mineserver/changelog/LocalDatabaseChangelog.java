package ru.sultanyarov.mineserver.changelog;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@ChangeLog(order = "001")
public class LocalDatabaseChangelog {
    @ChangeSet(order = "000", id = "dropDB", author = "vs", runAlways = true)
    public void dropDB(MongoDatabase database) {
        database.drop();
    }

    @ChangeSet(order = "001", id = "createTestUser", author = "vs", runAlways = true)
    public void createTestUser(MongoDatabase database) {
        database.createCollection("user");
        var user = String.format("{\n" +
                "   \"insert\":\"user\",\n" +
                "   \"documents\":[\n" +
                "      {\n" +
                "         \"name\":\"user\",\n" +
                "         \"password\":\"%s\"\n" +
                "      }\n" +
                "   ]\n" +
                "}", new BCryptPasswordEncoder().encode("user"));

        database.runCommand(Document.parse(user));
    }
}
